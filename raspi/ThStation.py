class ThStation:

    def __init__(self, new_id):
        self.id = new_id
        self.temp = None
        self.hum = None
        
    def reset(self):
        self.temp = None
        self.hum = None

    def getTemp(self):
        if self.temp is None:
            return None
        return self.temp

    def getHum(self):
        if self.hum is None:
            return None
        return self.hum
