class WindStation:

    def __init__(self):
        self.speed = None
        self.direction = None

    def reset(self):
        self.speed = None
        self.direction = None

    def get_speed(self):
        return self.speed

    def get_direction(self):
        return self.direction

