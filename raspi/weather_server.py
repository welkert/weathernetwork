import paho.mqtt.client as mqttClient
import paho.mqtt.publish as publish
import sys
import time
import psutil
import string
import random
import threading

from WeatherReport import *
from DataLog import *

def on_connect(client, userdata, flags, rc):
 
    if rc == 0:
 
        print("Connected to broker")
 
        global Connected                #Use global variable
        Connected = True                #Signal connection 

    else:
 
        print("Connection failed")

 
def on_message(client, userdata, message):
    
    print "Message received: "  + str(Connected)
    clientID=''

    for x in range(1,16):
        clientID+=random.choice(string.alphanum)
    
    print "---------------------------------------"
    #print "client: " + client
    #print "userdata: " + userdata
    #print "message: " + message
    #print "---------------------------------------"

    newVal = float(message.payload)
    print message.payload

def on_message_temp(client, userdata, message):
    #global WReport
    global DL

    print "Message received: " + str(Connected) + " to temperature"
    clientID=''

    for x in range(1,16):
        clientID+=random.choice(string.alphanum)

    print "------------------------------------"
    
    parts = message.payload.split()

    newVal = float(parts[1])
    print message.payload

    DL.updateTemp(parts[0], parts[1])

    #WReport.addTemp(newVal)

def on_message_hum(client, userdata, message):
    global DL

    print "Message received: " + str(Connected) + " to humidity"
    clientID=''

    for x in range(1,16):
        clientID+=random.choice(string.alphanum)

    print "-------------------------------------"
    
    parts = message.payload.split()

    newVal = float(parts[1])
    print message.payload

    #newVal = float(message.payload)
    #print message.payload

    #WReport.addHum(newVal)

    DL.updateHum(parts[0], parts[1])

def on_message_wind_spd(client, userdata, message):
    global DL
    
    print "Message received: " + str(Connected) + " to wind speed"
    clientID=''

    for x in range(1,16):
        clientID+=random.choice(string.alphanum)

    print "------------------------------------"
    
    parts = message.payload.split()

    newVal = float(parts[1])
    print message.payload

    DL.updateWindSpd(parts[1])

def on_message_wind_dir(client, userdata, message):
    global DL
    
    print "Message received: " + str(Connected) + " to wind direction"
    clientID=''

    for x in range(1,16):
        clientID+=random.choice(string.alphanum)

    print "-----------------------------------"
    
    parts = message.payload.split()
    newVal = float(parts[1])
    print message.payload

    DL.updateWindDir(parts[1])

def publishData():
    global timer
    global DL

    temp_message = DL.getTempMessage()
    hum_message = DL.getHumMessage()
    wind_message = DL.getWindMessage()
    avg_message = DL.getAvgMessage()

    # Send temperature message
    payload = temp_message

    try:
        publish.single(tempTopic, payload, hostname=mqttHost, transport=tTransport, port=tPort, auth={'username':mqttUsername, 'password':mqttAPIKey})
    
        print("Published temp data")


    except (KeyboardInterrupt):
        return

    except:
        print("There was an error while publishing temp data")

    


    # Send humidity message
    payload = hum_message

    try:
        publish.single(humTopic, payload, hostname=mqttHost, transport=tTransport, port=tPort, auth={'username':mqttUsername, 'password':mqttAPIKey})

        print("Published hum data")

    except (KeyboardInterrupt):
        return

    except:
        print("There was an error while publishing hum data")

    # Send wind message
    payload = wind_message

    try:
        publish.single(windTopic, payload, hostname=mqttHost, transport=tTransport, port=tPort, auth={'username':mqttUsername, 'password':mqttAPIKey})

        print("Published wind data")
    except (KeyboardInterrupt):
        return

    except:
        print("There was an error while publishing wind data")
        

    # Send Summary message
    payload = avg_message    

    try:
        publish.single(avgTopic, payload, hostname=mqttHost, transport=tTransport, port=tPort, auth={'username':mqttUsername, 'password':mqttAPIKey})

        print("Published avg data")
    except (KeyboardInterrupt):
        return

    except:
        print("There was an error while publishing avg data")


    DL.resetStations()
    timer = threading.Timer(60.0, publishData).start()

if __name__ == "__main__":

    BrokerUsername = sys.argv[1]
    print "Username: " + BrokerUsername

    DL = DataLog()

    timer = threading.Timer(60.0, publishData).start()

    ###############################	
    ## Parameters for the broker ##
    ###############################

    Connected = False

    broker_address="localhost"
    port = 12948                         #Broker port
    user = "yourUser"                    #Connection username
    password = "yourPassword"            #Connection password

    client = mqttClient.Client(BrokerUsername)               #create new instance
    client.username_pw_set(user, password=password)    #set username and password
    client.on_connect= on_connect                      #attach function to callback
    client.on_message= on_message                      #attach function to callback

    client.message_callback_add('weather/temp', on_message_temp)
    client.message_callback_add('weather/hum', on_message_hum)
    client.message_callback_add('weather/wind/spd', on_message_wind_spd)
    client.message_callback_add('weather/wind/dir', on_message_wind_dir)

    client.connect(broker_address)          #connect to broker
 
    client.loop_start()        #start the loop


    ########################################	
    ## Parameters for the ThingSpeak link ##
    ########################################

    #TotalStations = 2
    StationsReported = 0
    CurrentSum = 0

    string.alphanum='1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'

    # The ThingSpeak Channel ID.
    # Replace <YOUR-CHANNEL-ID> with your channel ID.
    tempChannelID = "548031"
    humChannelID = "548032"
    windChannelID = "548033"
    avgChannelID = "550362"

    # The WriteAPI Key for the channel.
    # Replace <YOUR-CHANNEL-WRITEAPIKEY> with your write API key.
    tempWriteAPIKey = "6UTAS13EN559XCY3"
    humWriteAPIKey = "6X0SVRLM9TOIMNHG"
    windWriteAPIKey = "X682KPFPZQ7MJYF9"
    avgWriteAPIKey = "SPUUN003PT7XMPAO"


    # The Hostname of the ThingSpeak MQTT broker.
    mqttHost = "mqtt.thingspeak.com"

    # You can use any Username.
    mqttUsername = "TSMQTTRpiDemo"

    # Your MQTT API Key from Account > My Profile.
    mqttAPIKey = "XXXXXXXXXXXXXXXX"

    tTransport = "websockets"
    tPort = 80

    # Create the topic string
    tempTopic = "channels/" + tempChannelID + "/publish/" + tempWriteAPIKey
    humTopic = "channels/" + humChannelID + "/publish/" + humWriteAPIKey
    windTopic = "channels/" + windChannelID + "/publish/" + windWriteAPIKey 
    avgTopic = "channels/" + windChannelID + "/publish/" + avgWriteAPIKey

    while Connected != True:    #Wait for connection
        time.sleep(0.1)

    client.subscribe("weather/#")

    try:
        while True:
            time.sleep(1)
 
    except KeyboardInterrupt:
        print "exiting"
        client.disconnect()
        client.loop_stop()
