class AverageTracker:

    def __init__(self):
        self.entries = []
    
    def addEntry(self, new_entry):
        self.entries.append(new_entry)

    def clearEntries(self):
        self.entries = []

    def calcAverage(self):
        sum = 0
        if len(self.entries) == 0:
            return 0
        else:
            for entry in self.entries:
                sum += entry

            return sum/len(self.entries)
