from AverageTracker import *

class WeatherReport:

    def __init__(self):
        self.temp = AverageTracker()
        self.hum = AverageTracker()
        self.wind_spd = None
        self.wind_dir = None

    def clearData(self):
        self.temp.clearEntries()
        self.hum.clearEntries()
        self.wind_spd = None
        self.wind_dir = None

    def addTemp(self, newTemp):
        self.temp.addEntry(newTemp)

    def addHum(self, newHum):
        self.hum.addEntry(newHum)

    def setWindSpd(self, new_spd):
        self.wind_spd = new_spd

    def setWindDir(self, new_dir):
        self.wind_dir = new_dir

    def setWind(self, new_spd, new_dir):
        self.wind_spd = new_spd
        self.wind_dir = new_dir

    def getTemp(self):
        return self.temp.calcAverage()

    def getHum(self):
        return self.hum.calcAverage()

    def getWindSpd(self):
        if self.wind_spd is None:
            return 0
        else:
            return self.wind_spd

    def getWindDir(self):
        if self.wind_dir is None:
            return 0 
        else:
            return self.wind_dir
