import paho.mqtt.client as mqttClient
import paho.mqtt.publish as publish
import sys
import time
import psutil
import string
import random

from WeatherReport import *

def on_connect(client, userdata, flags, rc):
 
    if rc == 0:
 
        print("Connected to broker")
 
        global Connected                #Use global variable
        Connected = True                #Signal connection 

    else:
 
        print("Connection failed")

 
def on_message(client, userdata, message):
    
    print "Message received: "  + str(Connected)
    clientID=''

    for x in range(1,16):
        clientID+=random.choice(string.alphanum)
    
    global StationsReported
    global CurrentSum
    global BrokerChannel

    StationsReported = StationsReported + 1
    newVal = float(message.payload)
    CurrentSum = CurrentSum + newVal

    print "----------------------------------------"
    print "StationsReported: " + str(StationsReported)
    print "TotalStations: " + str(TotalStations)
    print "CurrentSum: " + str(CurrentSum)
    print "----------------------------------------"

    if StationsReported >= TotalStations:
        print "Entered Loop"
        average = CurrentSum / TotalStations
        print "Average: " + str(average)
        CurrentSum = 0
        StationsReported = 0

        payload = "field1=" + str(average) + "&field2=" + str(average)

        try:
            publish.single(topic, payload, hostname=mqttHost, transport=tTransport, port=tPort, auth={'username':mqttUsername, 'password':mqttAPIKey})
            print("Published some data")
        except (KeyboardInterrupt):
            return

        except:
            print ("There was  an error while publishing the data")


if __name__ == "__main__":

    TotalStations = int(sys.argv[1])
    print "Total Stations: " + str(TotalStations) 

    BrokerChannel = sys.argv[2]
    print "Channel: " + BrokerChannel

    BrokerUsername = sys.argv[3]
    print "Username: " + BrokerUsername

    ###############################	
    ## Parameters for the broker ##
    ###############################

    Connected = False

    broker_address="localhost"
    port = 12948                         #Broker port
    user = "yourUser"                    #Connection username
    password = "yourPassword"            #Connection password

    client = mqttClient.Client(BrokerUsername)               #create new instance
    client.username_pw_set(user, password=password)    #set username and password
    client.on_connect= on_connect                      #attach function to callback
    client.on_message= on_message                      #attach function to callback
 
    client.connect(broker_address)          #connect to broker
 
    client.loop_start()        #start the loop


    ########################################	
    ## Parameters for the ThingSpeak link ##
    ########################################

    #TotalStations = 2
    StationsReported = 0
    CurrentSum = 0

    string.alphanum='1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'

    # The ThingSpeak Channel ID.
    # Replace <YOUR-CHANNEL-ID> with your channel ID.
    channelID = "540677"

    # The WriteAPI Key for the channel.
    # Replace <YOUR-CHANNEL-WRITEAPIKEY> with your write API key.
    writeAPIKey = "14S912H97HDLME3P"

    # The Hostname of the ThingSpeak MQTT broker.
    mqttHost = "mqtt.thingspeak.com"

    # You can use any Username.
    mqttUsername = "TSMQTTRpiDemo"

    # Your MQTT API Key from Account > My Profile.
    mqttAPIKey = "XXXXXXXXXXXXXXXX"

    tTransport = "websockets"
    tPort = 80

    # Create the topic string
    topic = "channels/" + channelID + "/publish/" + writeAPIKey


 
    while Connected != True:    #Wait for connection
        time.sleep(0.1)
 
    client.subscribe(BrokerChannel)

 
    try:
        while True:
            time.sleep(1)
 
    except KeyboardInterrupt:
        print "exiting"
        client.disconnect()
        client.loop_stop()
