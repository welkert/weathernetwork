from ThStation import *
from WindStation import *

num_thstations = 8

class DataLog:

    def __init__(self):
        
        self.temp_stations = []
        self.wind_station = WindStation()
        for i in xrange (0,num_thstations):
            self.temp_stations.append(ThStation(i))
    
    def resetStations(self):
        for i in xrange (0, num_thstations):
            self.temp_stations[i].reset()
        self.wind_station.reset()

    def updateTemp(self, station_id, newTemp):

        self.temp_stations[int(station_id)-1].temp = newTemp
    
    def updateHum(self, station_id, newHum):

        self.temp_stations[int(station_id)-1].hum = newHum

    def updateWindSpd(self, new_spd):
        
        self.wind_station.speed = new_spd

    def updateWindDir(self, new_dir):

        self.wind_station.direction = new_dir

    def getTempMessage(self):
        
        returnString = ""
        for i in xrange(0,8):
            if self.temp_stations[i].getTemp() is not None:
                if i > 0 and returnString != "":
                    returnString += "&"
                returnString += "field" + str(i+1) + "="
            
                returnString += str(self.temp_stations[i].getTemp())


        if returnString == "":
            return None

        return returnString

    def getHumMessage(self):

        returnString = ""
        for i in xrange(0,8):
            if self.temp_stations[i].getHum() is not None:
                if i > 0 and returnString != "":
                    returnString += "&"
                returnString += "field" + str(i+1) + "="

                returnString += str(self.temp_stations[i].getHum())
                        
        if returnString == "":
            return None

        return returnString

    def getWindMessage(self):

        returnString = ""
        if self.wind_station.get_speed() is not None:
            returnString += "field1=" + str(self.wind_station.get_speed())
        if self.wind_station.get_direction() is not None:
            if returnString != "":
                returnString += "&"
            returnString += "field2=" + str(self.wind_station.get_direction())

        if returnString == "":
            return None

        return returnString

    def getAvgMessage(self):

        returnString = ""
        avgTemp = None
        avgHum = None
        windSpd = self.wind_station.get_speed()
        windDir = self.wind_station.get_direction()

        subsum = 0.0
        count = 0.0

        for i in xrange(0,8):
            if self.temp_stations[i].getTemp() is not None:
                subsum += float(self.temp_stations[i].getTemp())
                count += 1.0

        if count != 0:
            avgTemp = float(subsum) / float(count)

        subsum = 0.0
        count = 0.0

        for i in xrange(0,8):
            if self.temp_stations[i].getHum() is not None:
                subsum += float(self.temp_stations[i].getHum())
                count += 1.0
        
        if count != 0:
            avgHum = float(subsum) / float(count)

        ## Debug statements
        #print "avgTemp" + str(avgTemp)
        #print "avgHum" + str(avgHum)
        #print "windSpd" + str(windSpd)
        #print "windDir" + str(windDir)

        if avgTemp is not None:
            returnString += "field1=" + str(avgTemp)
        if avgHum is not None:
            if returnString == "":
                returnString += "field2=" + str(avgHum)
            else:
                returnString += "&field2=" + str(avgHum)
        if windSpd is not None:
            if returnString == "":
                returnString += "field3=" + str(windSpd)
            else:
                returnString += "&field3=" + str(windSpd)
        if windDir is not None:
            if returnString == "":
                returnSTring += "field4=" + str(windDir)
            else:
                returnString += "&field4=" + str(windDir)

        if returnString == "":
            return None
        return returnString
